document.addEventListener("DOMContentLoaded", function () {
    document
    .getElementById("formulario")
    .addEventListener("submit", validarFormulario);
});
function validarFormulario(evento) {
    evento.preventDefault(); // Previene el comportamiento por defecto del evento (envío del formulario)
    fetch('00-Login-Register.xml') // con 'fetch' podemos obtener el archivo XML
    .then(response => response.text()) // Convierte la respuesta(el contenido de xml) a texto
    .then(data => {
        const parser = new DOMParser(); // Crea una instancia de DOMParser para analizar el XML
        const xmlDoc = parser.parseFromString(data, 'text/xml'); // Analiza el XML y obtiene un nuevo documento XML
        const usuarios = xmlDoc.getElementsByTagName('usuario'); // Obtiene todos los elementos 'usuario' del XML
        
        // Itera sobre los usuarios del archivo XML
        for (let i = 0; i < usuarios.length; i++) {
            const usuario = usuarios[i].getElementsByTagName('User')[0].textContent; // Obtiene el contenido del elemento 'User' para el usuario actual
            const clave = usuarios[i].getElementsByTagName('clave')[0].textContent; // Obtiene el contenido del elemento 'clave' para el usuario actual
            const redireccion = usuarios[i].getElementsByTagName('redireccion')[0].textContent; // Obtiene el contenido del elemento 'redireccion' 
                                                                                                //para el usuario actual que en ambos usuarios será el mismo redireccionamiento
            // Realiza la verificación directamente con los valores del archivo XML
            if (usuario === usuario && clave === clave) { // COMPROBACIÓN INCORRECTA: MISMA VARIABLE PARA COMPARAR Y GUARDAR
                window.location.href = redireccion; // Redirige al usuario a la página de redirección especificada en el archivo XML
                return; // Detiene la ejecución del bucle y la función
            }
        }
        console.log('Credenciales inválidas'); // Muestra un mensaje en la consola indicando que las credenciales son inválidas si es que no se colocan los datos correctos
        this.submit(); // ENVÍO DEL FORMULARIO - DEPENDE DEL CONTEXTO DE EJECUCIÓN
    })
}