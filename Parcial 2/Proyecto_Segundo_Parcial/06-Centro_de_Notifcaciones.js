// Obtener elementos del DOM
const notifications = document.querySelector('.notifications');
const deleteAllBtn = document.querySelector('.delete-all-btn');
// Función para cargar las notificaciones desde el archivo JSON
async function cargarNotificaciones() {
  try {
    const response = await fetch('notificaciones.json'); // Realiza una solicitud HTTP para obtener el archivo JSON
    const data = await response.json(); // Convierte la respuesta en un objeto JavaScript
    return data; // Devuelve los datos del archivo JSON
  } catch (error) {
    console.error('Error al cargar las notificaciones:', error); // Muestra un mensaje de error en caso de que haya un problema en la carga del archivo
    return []; // Devuelve un arreglo vacío en caso de error
  }
}
// Función para crear una notificación en el DOM
function crearNotificacion(descripcion) {
  const div = document.createElement('div'); // Crea un nuevo elemento div
  div.classList.add('notification'); // Agrega la clase 'notification' al elemento div
  div.innerHTML = `
    <p>${descripcion}</p>
    <button class="delete-btn">Borrar</button>
  `; // Establece el contenido HTML del elemento div con la descripción y el botón de borrado
  notifications.appendChild(div); // Agrega el elemento div como hijo del contenedor de notificaciones
  const deleteBtn = div.querySelector('.delete-btn'); // Busca el botón de borrado dentro del elemento div
  deleteBtn.addEventListener('click', () => {
    div.remove(); // Elimina el elemento div al hacer clic en el botón de borrado
  });
}
// Invocar la función para cargar y crear las notificaciones al cargar la página
cargarNotificaciones().then((notificaciones) => {
  notificaciones.forEach((notificacion) => {
    crearNotificacion(notificacion.descripcion); // Crea una notificación en el DOM para cada notificación cargada desde el archivo JSON
  });
});
// Agregar listener para borrar todas las notificaciones
deleteAllBtn.addEventListener('click', () => {
  notifications.innerHTML = ''; // Elimina todas las notificaciones estableciendo el contenido HTML del contenedor de notificaciones como una cadena vacía
});