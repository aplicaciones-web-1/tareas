import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FirebaseCodeErrorService {

  constructor() { }
  codeError(code: string){
    switch(code){

      //el correo ya existe
      case 'auth/email-already-in-use':
        return 'El usuario ya existe';
        // contraseña debil
        case 'auth/weak-password':
        return 'La contraseña es muy debil';
        //correo invalido
        case 'auth/invalid-email':
        return 'Correo no válido';

        //contraseña incorrecta
        case 'auth/wrong-password':
          return 'Contraseña Incorrecta';

          //el usuario no existe
        case 'auth/user-not-found':
          return 'El usuario no existe';
        default:
        return 'Error Desconocido'
    }
  }
}
