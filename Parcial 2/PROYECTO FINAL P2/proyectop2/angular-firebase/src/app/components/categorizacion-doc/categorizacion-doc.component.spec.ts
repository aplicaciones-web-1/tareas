import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorizacionDocComponent } from './categorizacion-doc.component';

describe('CategorizacionDocComponent', () => {
  let component: CategorizacionDocComponent;
  let fixture: ComponentFixture<CategorizacionDocComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CategorizacionDocComponent]
    });
    fixture = TestBed.createComponent(CategorizacionDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
