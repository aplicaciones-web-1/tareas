import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categorizacion-doc',
  templateUrl: './categorizacion-doc.component.html',
  styleUrls: ['./categorizacion-doc.component.css']
})
export class CategorizacionDocComponent {
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router
  ){}

  ngOnInit():void{
    
  }
  logOut(){
    this.afAuth.signOut().then(()=>this.router.navigate(['/login']));
  }
}
