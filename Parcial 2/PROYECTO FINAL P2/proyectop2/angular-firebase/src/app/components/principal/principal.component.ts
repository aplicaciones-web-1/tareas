import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth'
import { Router } from '@angular/router';  

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent {
  dataUser: any;
  ///
  carrerasVisible: boolean = false;
///
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router
  ){}

  ngOnInit():void{
    this.afAuth.currentUser.then(user =>{
      if(user && user.emailVerified){
        this.dataUser = user;
      }else{
        this.router.navigate(['/login']);
      }
    })
  }
  logOut(){
    this.afAuth.signOut().then(()=>this.router.navigate(['/login']));
  }
///para ver el acordeon
toggleCarreras() {
  this.carrerasVisible = !this.carrerasVisible;
}

toggleCarrerasHover() {
  this.carrerasVisible = true;
}

toggleCarrerasLeave() {
  this.carrerasVisible = false;
}
///


}
