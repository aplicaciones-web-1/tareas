import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionDocumentosComponent } from './edicion-documentos.component';

describe('EdicionDocumentosComponent', () => {
  let component: EdicionDocumentosComponent;
  let fixture: ComponentFixture<EdicionDocumentosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EdicionDocumentosComponent]
    });
    fixture = TestBed.createComponent(EdicionDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
