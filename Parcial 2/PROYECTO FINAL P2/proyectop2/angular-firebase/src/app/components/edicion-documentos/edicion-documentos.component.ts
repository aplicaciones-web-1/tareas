import { Component, OnInit } from '@angular/core';
import { Storage, ref, uploadBytes, listAll, getDownloadURL, deleteObject } from '@angular/fire/storage';

@Component({
  selector: 'app-edicion-documentos',
  templateUrl: './edicion-documentos.component.html',
  styleUrls: ['./edicion-documentos.component.css']
})
export class EdicionDocumentosComponent implements OnInit {
  documentos: { name: string; url: string; comentarios: { comentario: string; editando: boolean; }; }[] = [];

  constructor(private storage: Storage) { }

  ngOnInit() {
    this.getDoc();
  }

  uploadDoc($event: any) {
    const file = $event.target.files[0];
    console.log(file);

    const docRef = ref(this.storage, `Documentos/${file.name}`);

    uploadBytes(docRef, file)
      .then(response => {
        console.log(response);
        // Agregar el documento a la lista sin recargar la página
        getDownloadURL(docRef)
          .then(url => {
            this.documentos.push({ name: file.name, url: url, comentarios: { comentario: '', editando: false } });
          })
          .catch(error => console.log(error));
      })
      .catch(error => console.log(error));
  }

  getDoc() {
    const documentRef = ref(this.storage, 'Documentos');
    listAll(documentRef)
      .then(async response => {
        for (let item of response.items) {
          getDownloadURL(item)
            .then(url => {
              this.documentos.push({ name: item.name, url: url, comentarios: { comentario: '', editando: false } });
            })
            .catch(error => console.log(error));
        }
      })
      .catch(error => console.log(error));
  }

  redireccionar(url: string) {
    window.open(url, '_blank');
  }

  eliminarDocumento(documento: { name: string; url: string; comentarios: { comentario: string; editando: boolean; }; }) {
    const docRef = ref(this.storage, `Documentos/${documento.name}`);
    deleteObject(docRef)
      .then(() => {
        console.log('Documento eliminado');
        this.documentos = this.documentos.filter(doc => doc !== documento);
      })
      .catch(error => console.log(error));
  }

  editarComentario(documento: { name: string; url: string; comentarios: { comentario: string; editando: boolean; }; }) {
    documento.comentarios.editando = true;
  }

  guardarComentario(documento: { name: string; url: string; comentarios: { comentario: string; editando: boolean; }; }) {
    documento.comentarios.editando = false;
  }
  
}
