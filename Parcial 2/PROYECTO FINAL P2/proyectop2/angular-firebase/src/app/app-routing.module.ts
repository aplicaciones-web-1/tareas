import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegistrarUsuarioComponent } from './components/registrar-usuario/registrar-usuario.component';
import { RecuperarPasswordComponent } from './components/recuperar-password/recuperar-password.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { VerificarCorreoComponent } from './components/verificar-correo/verificar-correo.component';
import { CategorizacionDocComponent } from './components/categorizacion-doc/categorizacion-doc.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { EdicionDocumentosComponent } from './components/edicion-documentos/edicion-documentos.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
{ path: 'login', component: LoginComponent},
{ path: 'registrar-usuario', component: RegistrarUsuarioComponent},
{ path: 'recuperar-password', component: RecuperarPasswordComponent},
{ path: 'principal', component: PrincipalComponent},
{ path: 'verificar-correo', component: VerificarCorreoComponent},
{ path: 'categorizacion-doc', component: CategorizacionDocComponent},
{ path: 'perfil', component: PerfilComponent},
{ path: 'edicion-documentos', component: EdicionDocumentosComponent},

{path: '**', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
