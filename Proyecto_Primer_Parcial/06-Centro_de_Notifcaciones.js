// Obtener elementos del DOM
const notifications = document.querySelector('.notifications');
const deleteAllBtn = document.querySelector('.delete-all-btn');
const deleteBtns = document.querySelectorAll('.delete-btn');

// Agregar listeners para borrar notificaciones individualmente
deleteBtns.forEach((btn) => {
  btn.addEventListener('click', (e) => {
    e.target.parentElement.remove();
  });
});

// Agregar listener para borrar todas las notificaciones
deleteAllBtn.addEventListener('click', () => {
  notifications.innerHTML = '';
});
