//funcion para actualizar informacion
function updateInfo() {
    var email = document.getElementById("email").value;
    var username = document.getElementById("username").value;
    var faculty = document.getElementById("faculty").value;
    
    // Validar el campo de correo electrónico
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(email)) {
      alert("Por favor, ingrese una dirección de correo electrónico válida.");
      return;
    }
        // Validar el campo de facultad
    var facultyvar =/^[a-zA-Z]+(([',. -][a-zA-Z ]+)*[a-zA-Z]*)*$/;
    if (!facultyvar.test(faculty)) {
      alert("Por favor, ingrese una Facultd valida");
      return;
    }
    // Validar el campo de nombre de usuario
    var namePattern = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
    if (!namePattern.test(username)) {
      alert("Por favor, ingrese un nombre de usuario válido (solo letras y espacios).");
      return;
    }
    document.getElementById("email").value = email;
  document.getElementById("username").value = username;
  document.getElementById("faculty").value = faculty;
    alert("La información se ha actualizado con éxito.");
  }
  //Funcion para subir imagen
  function mostrarImagen() {
    const input = document.getElementById('photo');
    const imagenDiv = document.getElementById('imagenDiv');
  
    const file = input.files[0];
    const reader = new FileReader();
  
    reader.addEventListener('load', function() {
      const imagen = new Image();
      imagen.src = reader.result;
      imagenDiv.appendChild(imagen);
    }, false);
  
    if (file) {
      reader.readAsDataURL(file);
    }
  }